console.log(`Usage of JavaScript Symbols and well-known Symbols`);

// enums

// eg. safety recommendations
let colorCodes = {
    RED: Symbol('RED'),
    ORANGE: Symbol('ORANGE'),
    YELLOW: Symbol('YELLOW'),
    GREEN: Symbol('GREEN')
};

// eg. directions
let directions = {
    UP: Symbol('UP'),
    DOWN: Symbol('DOWN'),
    LEFT: Symbol('LEFT'),
    RIGHT: Symbol('RIGHT')
};





// semi-private fields
// const _note_key = Symbol('key');
// const _note_title = Symbol('title');
// const _note_body = Symbol('body');

// class Note {
//     constructor(key, title, body) {
//         this[_note_key] = key;
//         this[_note_title] = title;
//         this[_note_body] = body;
//     }

//     get key() { return this[_note_key]; }
//     get title() { return this[_note_title]; }
//     set title(newTitle) { this[_note_title] = newTitle; }
//     get body() { return this[_note_body]; }
//     set body(newBody) { this[_note_body] = newBody; }   
// };

let Note = (function () {
    const _note_key = Symbol('key');
    const _note_title = Symbol('title');
    const _note_body = Symbol('body');

    class Note {
        constructor(key, title, body) {
            this[_note_key] = key;
            this[_note_title] = title;
            this[_note_body] = body;
        }

        get key() { return this[_note_key]; }
        get title() { return this[_note_title]; }
        set title(newTitle) { this[_note_title] = newTitle; }
        get body() { return this[_note_body]; }
        set body(newBody) { this[_note_body] = newBody; }
    };

    return Note;
})();

// import {Note} from './lib/Note.js';

let note = new Note(123, 'Title', 'Body');
note.body = 'new Body';
// note.key = 234;
console.log(note.body);

Object.getOwnPropertySymbols(note).forEach((symbol) => {
    console.log(note[symbol]);
})








// Well-Known Symbols (System Symbols)


// Symbol.hasInstance: instanceof
class AdvArray {
    static [Symbol.hasInstance](obj) {
        console.log(`Symbol.hasInstance`);
        return Array.isArray(obj);
    }
}
console.log([] instanceof AdvArray);
console.log(`--------------------------------------`);



// Symbol.iterator: for...of
class NumberCollection {
    *[Symbol.iterator]() {
        var i = 0;
        while (this[i] !== undefined) {
            yield this[i];
            ++i;
        }
    }

}

let numCollection = new NumberCollection();
numCollection[0] = 1;
numCollection[1] = 2;
numCollection[2] = 3;
for (let num of numCollection) {
    console.log(num); 
}
console.log(`--------------------------------------`);



// Symbol.replace: String.replace
class Replaceer {
    constructor(value) {
      this.value = value;
    }

    [Symbol.replace](string, searchString) {
        console.log(`Current string: '${string}', searchString: '${searchString}' replace with: '${this.value}' + 108 at the end`);

        return string.replace(searchString, `${this.value}108`); 
    }
}
  
console.log('foo'.replace(new Replaceer('bar'), 'oo'));
console.log(`--------------------------------------`);




// Symbol.search: String.search
class Searcher {
    constructor(value) {
        this.value = value;
    }
    [Symbol.search](string) {
        // returns position counted from 1
        return string.indexOf(this.value) + 1;
    }
}
var fooSearch = 'foobar'.search(new Searcher('foo'));
var barSearch = 'foobar'.search(new Searcher('bar'));
var bazSearch = 'foobar'.search(new Searcher('baz'));
console.log(fooSearch);
console.log(barSearch);
console.log(bazSearch);
console.log(`--------------------------------------`);




// Symbol.toPrimitive: ++variable, Number(variable), '' + variable, String(variable)
var obj1 = {};
console.log(+obj1);     // NaN
console.log(`${obj1}`); // "[object Object]"
console.log(obj1 + ""); // "[object Object]"

class SuperObject {
    constructor(val) {
        this.val = val;
    }
    [Symbol.toPrimitive](hint) {
        if (hint === 'string') {
            return `string`;
        } else if (hint === 'number') {
            return 108;
        } else if (hint === 'default') {
            return 'default';
        }else {
            // the rest type
            return -1;
        }
    }
}

let obj = new SuperObject();
console.log(+obj);     
console.log(`${obj}`); 
console.log(obj + ""); 

console.log(+obj === 108);
console.log(Number(obj) === 108); ;
console.log(`${obj}` === 'string');
console.log(String(obj) === 'string');
console.log(obj + '');
console.log(Object(obj) === 'default');







class Collection {

    get [Symbol.toStringTag]() {
      return 'Collection';
    }

    toString() {
        return `It's collection :)`;
    }
  
}
let collection = new Collection();
console.log(collection.toString());
console.log(Object.prototype.toString.call(collection));



// GENERATOR FUNCTIONS
function* idMaker() {
    var index = 0;
    while(index < 3){
        yield index++;
    }

}

var gen = idMaker();

console.log(gen.next()); // 0
console.log(gen.next().value); // 1
console.log(gen.next()); // 2
console.log(gen.next()); // done: true