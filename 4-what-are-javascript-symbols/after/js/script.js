console.log(`What are JavaScript Symbols?`);

// Symbols are considered as primitive type introduced in ES6.
let num = 3; // instead of Number(3);
let str = 'string'; // instead of String('string')
let bool = true; // instead of Boolean(true)

console.log(num);
console.log(str);
console.log(bool);

// Symbols are not Objects
// let sym = new Symbol();
let sym = Symbol();
console.log(sym instanceof Object); //

// but Symbols are not clear primitive type neither
// +Symbol() // Uncaught TypeError: Cannot convert a Symbol value to a number
// '' + Symbol() Uncaught TypeError: Cannot convert a Symbol value to a string
// Symbol() + 'string' // Uncaught TypeError: Cannot convert a Symbol value to a string
// console.log(`${Symbol()}`); Uncaught TypeError: Cannot convert a Symbol value to a string

// there is no Symbol shortcut definition like Numbers, Strings or Booleans
let symbol = Symbol() // #1506148914518485914635489645314856
console.log(symbol);
console.log(typeof symbol); 
console.log(symbol.toString())

// symbols with label
// - label does not impact on Symbol value
// - label is helpful for debugging and understanding what does the Symbol corespond to
let symbolLabel = Symbol('label')
console.log(symbolLabel);
console.log(symbolLabel.description);
console.log(`-------------------------------------------`);







// Symbols look like the same, but they have differnt values
const sym1 = Symbol();
const sym2 = Symbol();

console.log(sym1, sym2);
console.log(sym1 === sym2);


// even Symbols with the same labels looks the same, but they have completly differnt values
const symA = Symbol('special');
const symB = Symbol('special');

console.log(symA, symB);
console.log(sym1 === sym2);
console.log(`-------------------------------------------`);






// A symbol value may be used as an identifier for object properties; this is the data type's primary purpose
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol

// we want to add a special ID to user eg. ID from other system
const idSym = Symbol('id');

let user = {
    id: 1234,
    name: 'Hans',
    city: 'Warsaw',
    age: 20,
    idSym: 1234567890, // it does not work, because its treated as a String, and we could overwrite some data
    [idSym]: 1234567890,
}

// it overwrites the primary id of the user, and we cannot save the special ID this way, because we dont know, what are the properties names of the 'user' Object (eg. fetched by AJAX request)
// user.id = 1234567890;
// user[idSym] = 1234567890; // proper way to save the special ID

console.log(user);
console.log(user[idSym]);
console.log(`-------------------------------------------`);






// The following operations ignore symbols as property keys:
// - Object.keys()
Object.keys(user).forEach((key) => {
    console.log(key);
})
console.log(`-------------------------------------------`);
// - for...in loop
for (const key in user) {
    console.log(`Key: ${key}, Value: ${user[key]}`);
}
console.log(`-------------------------------------------`);
// - Object.getOwnPropertyNames()
Object.getOwnPropertyNames(user).forEach((key) => {
    console.log(key);
})
console.log(`-------------------------------------------`);
// - JSON.stringify()
console.log(JSON.stringify(user));                 
console.log(`-------------------------------------------`);





// The following operations are aware of symbols as property keys:
// - Reflect.ownKeys()
Reflect.ownKeys(user).forEach((key) => {
    console.log(key);
})
console.log(`-------------------------------------------`);
// - Object.assign()
const manId = Symbol('id');
let man = { 
    name: 'Hans',
    [manId]: 987654321,
};
let copyOfMan = Object.assign({}, man);
console.log(copyOfMan); // {name: "Hans", Symbol(id): 987654321}
console.log(copyOfMan[manId]);
console.log(man[manId] === copyOfMan[manId]);
console.log(`-------------------------------------------`);
// - Object.getOwnPropertySymbols()
Object.getOwnPropertySymbols(user).forEach((key) => {
    console.log(key);
})
console.log(`-------------------------------------------`);






// global Symbol registry
const symX = Symbol.for('secret');
const symY = Symbol.for('secret');

// global Symbols with the same label are equal => the label is a reference of Symbol
console.log(symX === symY);
console.log(`-------------------------------------------`);


// local Symbol VS global Symbol
let localSymbol = Symbol('superSymbol');
let globalSymbol = Symbol.for('superSymbol');

console.log(Symbol.keyFor(localSymbol));
console.log(Symbol.keyFor(globalSymbol));
console.log(Symbol.keyFor(localSymbol) === undefined);
console.log(Symbol.keyFor(globalSymbol) === 'superSymbol');
console.log(Symbol.for(Symbol.keyFor(globalSymbol)) === Symbol.for('superSymbol'));





// What Symbols are, what Symbols aren’t:
// - Symbols will never conflict with Object string keys
// - Symbols are not private -> Object.getOwnPropertySymbols()
// Symbols are not coercible into primitives -> +Symbol(), '' + Symbol(), Symbol() + 'string', `${Symbol()}`
// - Symbols are not always unique -> Symbol.for()
// - Enumerable Symbols can be copied to other objects -> Object.assign() will copy Symbols to the other object