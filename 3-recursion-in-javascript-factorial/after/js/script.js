console.log(`Recursion in JavaScript (factorial)`);

// factiorial
// 0! = 1
// 1! = 1
// 2! = 1 * 2
// 3! = 1 * 2 * 3
// 4! = 1 * 2 * 3 * 4
// 5! = 1 * 2 * 3 * 4 * 5


// factorial without recursion
// function factorial(n) {
//     let result = 1;

//     if (n < 0) {
//         throw new Error('There is no factorial of negative numbers!');
//     }

//     for (let i=1; i<=n; ++i) {
//         result *= i;
//     }

//     return result;
// }


// factorial using recursion
// const factorial = (n) => {

//     if (n < 0) {
//         throw new Error('There is no factorial of negative numbers!');
//     } else if (n === 0) {
//         return 1;
//     }

//     return n * factorial(n - 1);
// }


// factorial using recursion => one line function
const factorial = n => (n < 0) ? -1 : ( (n === 0) ? 1 : n * factorial(n - 1) );


// console.log( factorial(-2) );
console.log( factorial(0) );
console.log( factorial(3) );