console.log(`JavaScript FAQ: Ternary operator`);

// condition ? expression-true : expression-false;

let height = 185;
// let heightDesc = null;

// if (height > 180) {
//     heightDesc = 'tall';
// } else {
//     console.log(`run command before SHORT`);
//     heightDesc = 'short';
// }

let heightDesc = (height > 180) ? 'tall' : (console.log(`run command before SHORT`), 'short');

heightDesc = 'medium';

// ternary operator without 'else' part
// heightDesc = (height > 180) ? 'tall';
// heightDesc = (height > 180) ? 'tall' : ;
heightDesc = (height > 180) ? heightDesc : 'short' ;

console.log(`He is ${heightDesc}`);

// console.log(`He is ${(height > 180) ? 'tall' : 'short'}`);