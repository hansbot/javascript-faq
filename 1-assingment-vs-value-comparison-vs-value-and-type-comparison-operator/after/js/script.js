console.log(`JavaScript FAQ: = vs == vs ===`);

let num = 108;
let numString = "108";

// = assingment operator
// if (num = numString) {
//     console.log(`EQUAL`);
// }

// == value comparison operator
// if (num == numString) {
//     console.log(`EQUAL`);
// }

// === value and type comparison operator
if (num === numString) {
    console.log(`EQUAL`);
}